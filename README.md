# Biljard #

### Namen repozitorija ###

Biljard je projekt v Javi pri predmetu Programiranje 2, š.leto 2014/2015. Cilj projekta je ustvariti čim bolj natančno simulacijo odbojev žog v ravnini. 


### Vsebina repozitorija ###

Repozitorij vsebuje: 



	* animacija
		* `Animation.java`: vsebuje glavni program, in razred za glavno okno
		* `Table.java`: grafični del programa
		* `Ball.java`: vsebuje razred, ki hrani podatke žog, in ki simulira premike in odboje.