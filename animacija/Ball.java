package animacija;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.geom.Point2D;

/**
 *
 * @author Teo
 */
public class Ball {
    
    // fields
    private double posX = 0;      // default: 0 !!
    private double posY = 0;      // default: 0
    
    double radius = 25;
    private double mass = 1;
    
    private double velocity = 0;          // pixels per sec.
    private double velocityX;
    private double velocityY;
    private double velocityAngle = 0;
    
    private int number;
    private Color color = Color.BLACK;
    
    // constants
    private static double minimumVelocity = 10;
    private static double tableFriction = 0; // 1;  // decceleration: pixels / sec^2
    private static double airFriction = 0; // 0.6;  // weighted air friction (empirically)
    
    
    // constructors
    public Ball(double posX, double posY){
        this.posX = posX;
        this.posY = posY;
        setVelocityComponents();
    }

    public Ball(double posX, double posY, double mass, double velocity, double velocityAngle) {
        this.posX = posX;
        this.posY = posY;
        
        this.mass = mass;
        this.velocity = velocity;
        this.velocityAngle = velocityAngle;
        
        setVelocityComponents();
    }

    public Ball(double posX, double posY, double mass, double velocity, double velocityAngle, int number) {
        this.posX = posX;
        this.posY = posY;
        this.mass = mass;
        this.velocity = velocity;
        this.velocityAngle = velocityAngle;
        this.number = number;
        
        setVelocityComponents();
    }
    
    private void setVelocityComponents(){
        velocityX = velocity * Math.cos(velocityAngle);
        setVelocityY(- velocity * Math.sin(velocityAngle));
        
    }
    
    public void move(double dt){
        posX += dt * velocityX;
        posY += dt * velocityY;
        double frictionResistance = tableFriction;   // force / mass;
        double airResistance = velocity * airFriction;
        velocity = velocity - dt * frictionResistance;
        velocity = velocity - dt * airResistance/ mass;
        setVelocityComponents();
    }
    
    public double collisionTime(Ball other){  
        double x01 = posX;
        double x02 = other.getPosX();
        double y01 = posY;
        double y02 = other.getPosY();
        double vx1 = velocityX;
        double vx2 = other.getVelocityX();
        double vy1 = velocityY;
        double vy2 = other.getVelocityY();                                                            // x1(t) = x01 + vx1 * t, ...
        double r = (radius + other.getRadius());                   // we need the first solution for equation: sqrt( (x1(t) - x2(t))^2 + (y1(t) - y2(t))^2 ) == r1 + r2 (=r)
        
        
        double a = vx1*vx1 - 2 * vx1* vx2 + vx2*vx2 + vy1*vy1 - 2 * vy1 * vy2 + vy2 * vy2;
        double b = 2 * vx1 * x01 - 2 * vx2 * x01 - 2 * vx1 * x02 + 2 * vx2 * x02 + 2 * vy1 * y01 - 2 * vy2 * y01 - 2 * vy1 * y02 + 2 * vy2 * y02;
        double c = - r*r + x01*x01 - 2 * x01 * x02 + x02*x02 + y01*y01 - 2 * y01 * y02 + y02*y02;
        double collisionTime;
        double otherSolution; // to the square equation
        if (Math.pow(b, 2) - 4 * a * c >= 0){
            collisionTime = (-b - Math.sqrt(Math.pow(b, 2) - 4 * a * c)) / (2 * a);             // if balls do not collide, the square root is not defined
            otherSolution = (-b + Math.sqrt(Math.pow(b, 2) - 4 * a * c)) / (2 * a);
        } else{
            collisionTime = 100000000;
            return collisionTime;
        }
        if ((x01 - x02)*(x01 - x02) + (y01 - y02)*(y01 - y02) < r*r){         // fixes of the bug where balls dont collide but slide through each other
            if (otherSolution > Math.abs(collisionTime)){return 0;}           // could raise exception if balls nearly miss each other. 
        }
        
        return collisionTime;
    }
    
    public double collisionTime(Table table, double dt){
        double vX = velocityX;
        double vY = velocityY;
        double x = posX;
        double y = posY;
        int leftBorder = table.getBorderWidth();
        int topBorder = table.getBorderWidth();
        int rightBorder = table.getBorderWidth() + table.getWidth2();
        int botBorder = table.getBorderWidth() + table.getHeight2();
        
        if (x - radius < leftBorder){   // ball b hits left border of table
            return 0;
        } else if (x + radius > rightBorder){    // ball hits right border
            return 0;
        } else if(y - radius < topBorder){
            return 0;
        } else if(y + radius > botBorder){
            return 0;
        }
        
        double timeOfColisionTop = dt + 1;        // something bigger then dt
        double timeOfColisionBot = dt + 1;
        double timeOfColisionLeft = dt + 1;
        double timeOfColisionRight = dt + 1;
        
        if (x + vX * dt - radius < leftBorder){   // ball b hits left border of table
            timeOfColisionLeft = Math.abs((double)(x - radius - leftBorder) / vX);     // between 0 and dt
        } else if (x + vX * dt + radius > rightBorder){    // ball hits right border
            timeOfColisionRight = Math.abs((double)(x + radius - rightBorder) / vX);     // between 0 and dt
        } else if(y + vY * dt - radius < topBorder){
            timeOfColisionTop = Math.abs((double)(y - radius - topBorder) / vY);     // between 0 and dt
        } else if(y + vY * dt + radius > botBorder){
            timeOfColisionBot = Math.abs((double)(y + radius - botBorder) / vY); 
        }
        return Math.min(  Math.min(timeOfColisionBot, timeOfColisionLeft),    Math.min(timeOfColisionRight, timeOfColisionTop)   );    // the first time if there are more then one
    }
    
    public void collisionWith(Ball other){        
        double collisionTime = this.collisionTime(other);          // chech if necessary!!
        move(collisionTime);
        other.move(collisionTime);
        double x1 = posX; // cian
        double y1 = posY;
        double x2 = other.getPosX();
        double y2 = other.getPosY();
        
        double m1 = mass;
        double m2 = other.getMass();
        double v1 = velocity;
        double v2 = other.getVelocity();
        double fi1 = velocityAngle;
        double fi2 = other.getVelocityAngle();
        double alfa = Math.atan2(y1 - y2, x2 - x1);
  
        double v1o = Math.sin(alfa - fi1) * v1;  // velocity component orthogonal to the angle of collision
        double v2o = Math.sin(alfa - fi2) * v2;  // velocity component orthogonal to the angle of collision
        
        double v1p = Math.cos(alfa - fi1) * v1;
        double v2p = Math.cos(alfa - fi2) * v2;  // velocity component parallel to the angle of collision
        
        double[] afterCollisionVelocities = elasticCollision(m1, v1p, m2, v2p);  // calculates velocity components parallel to the angle of collision
        v1p = afterCollisionVelocities[0];
        v2p = afterCollisionVelocities[1];
        
        double v1xk = v1p * Math.cos(alfa) + v1o * Math.cos(alfa - Math.PI/2);
        double v1yk = - (v1p * Math.sin(alfa) + v1o * Math.sin(alfa - Math.PI/2));
        
        double v2xk = v2p * Math.cos(alfa) + v2o * Math.cos(alfa - Math.PI/2);
        double v2yk = - (v2p * Math.sin(alfa) + v2o * Math.sin(alfa - Math.PI/2));
        
        setVelocityX(v1xk);
        setVelocityY(v1yk);
        other.setVelocityX(v2xk);
        other.setVelocityY(v2yk);
    }
    
    public void collisionWith(Table table){
        double vX = velocityX;
        double vY = velocityY;
        double x = posX;
        double y = posY;
        int leftBorder = table.getBorderWidth();
        int topBorder = table.getBorderWidth();
        int rightBorder = table.getBorderWidth() + table.getWidth2();
        int botBorder = table.getBorderWidth() + table.getHeight2();
        
        if (x - radius <= leftBorder){   // ball b hits left border of table
            vX = Math.abs(vX);
            setVelocityX(vX);
        } else if (x + radius >= rightBorder){    // ball hits right border
            vX = - Math.abs(vX);
            setVelocityX(vX);
        } else if(y - radius <= topBorder){
            vY = Math.abs(vY);
            setVelocityY(vY);
        } else if(y + radius >= botBorder){
            vY = - Math.abs(vY);
            setVelocityY(vY);
        }
    }
    
    public double[] elasticCollision(double m1, double v1, double m2, double v2){
        double v1k = (v1 * (m1 - m2) + 2 * m2 * v2 ) / (m1 + m2);
        double v2k = (v2 * (m2 - m1) + 2 * m1 * v1 ) / (m1 + m2);
        double[] newVelocities = {v1k, v2k};
        return newVelocities;
    }
    
    public double distance(Ball other){
        return getPos().distance(other.getPos()) - radius - other.getRadius();
    }
    
    public Point2D getPos(){
        return new Point2D.Double(posX ,posY);
    }

    public double getPosX() {
        return posX;
    }

    public double getPosY() {
        return posY;
    }

    public double getRadius() {
        return radius;
    }

    public double getMass() {
        return this.mass;
    }

    public double getVelocityAngle() {
        return velocityAngle;
    }

    public double getVelocity() {
        return velocity;
    }

    public double getVelocityX() {
        return velocityX;
    }

    public double getVelocityY() {
        return velocityY;
    }

    public void setPosX(double posX) {
        this.posX = posX;
    }

    public void setPosY(double posY) {
        this.posY = posY;
    }

    public void setVelocityX(double velocityX) {
        this.velocityX = velocityX;
        velocity = Math.sqrt(velocityX*velocityX + velocityY*velocityY);
        
        if (this.velocityY == 0 && velocityX == 0){
            velocityAngle = 0;
            return;
        }
        
        velocityAngle = Math.atan2(- velocityY, velocityX);
        if (velocity < minimumVelocity){
            this.velocityX = 0;
            velocityY = 0;
            velocity = 0;
        }
    }

    public void setVelocityY(double velocityY) {
        this.velocityY = velocityY;
        velocity = Math.sqrt(velocityX*velocityX + velocityY*velocityY);
        
        if (this.velocityY == 0 && velocityX == 0){
            velocityAngle = 0;
            return;
        }
        
        velocityAngle = Math.atan2(- velocityY, velocityX);
        
        if (velocity < minimumVelocity){
            this.velocityX = 0;
            this.velocityY = 0;
            velocity = 0;
        }
    }

    public void setVelocityAngle(double velocityAngle) {
		this.velocityAngle = velocityAngle;
		setVelocityComponents();
	}

	public void setVelocity(double velocity) {
		this.velocity = velocity;
		setVelocityComponents();
	}

	public void setColor(Color color) {
        this.color = color;
    }
    
    public void draw(Graphics g){
    	g.setColor(color);
    	g.fillOval((int) posX - (int) radius, (int)posY - (int) radius, 2 * (int)radius, 2 * (int) radius);
        g.setColor(Color.WHITE);
        g.drawString(Integer.toString((int) mass), (int) posX, (int) posY);     // draws mass of the boll in the middle of it
    }
    
    
    
}

