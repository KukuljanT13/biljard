package animacija;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import javax.swing.JPanel;

/**
 *
 * @author Teo
 */
@SuppressWarnings("serial")
public class Table extends JPanel{
    
    // fields
    private int width;
    private int height;
    private ArrayList<Ball> allBalls = new ArrayList();
    private int borderWidth = 30;
    private Color bgColor = new Color(255, 255, 255);
    private Color borderColor;
    
    public ArrayList<Ball> toDraw = new ArrayList();

    public Table(int width, int height) {
    	super();
        this.width = width;
        this.height = height;
        setBackground(bgColor);
    }
    
    public Table(int width, int height, int borderWidth) {
    	super();
        this.width = width;
        this.height = height;
        this.borderWidth = borderWidth;
        setBackground(bgColor);
    }
    
    public void addBall(Ball ball){
        for (Ball other : allBalls){
            if (ball.getPos().distance(other.getPos()) < ball.getRadius() + other.getRadius()){
                return;
            }
        }
        allBalls.add(ball);
    }
    
    public void move(double dt){
    	while (true){
	        Ball[] collision = null;          // should always be overweitten
	        double earliestTime = dt;
	        boolean noCollisions = true;
	        for (int ballIndex = 0; ballIndex < allBalls.size() - 1; ballIndex++){    // goes through all but last Ball in the arrayList. 
	            double t2 = allBalls.get(ballIndex).collisionTime(this, dt);
	            if (t2 < earliestTime){
	                Ball[] collision2 = {allBalls.get(ballIndex)};
	                collision = collision2;
	                earliestTime = t2;
	                noCollisions = false;
	            }
	            for (int otherIndex = ballIndex+1; otherIndex < allBalls.size(); otherIndex++){
	                double t1 = allBalls.get(ballIndex).collisionTime(allBalls.get(otherIndex));
	                if (t1 < earliestTime && t1 >= 0){
	                    Ball[] collision2 = {allBalls.get(ballIndex), allBalls.get(otherIndex)};
	                    collision = collision2;
	                    earliestTime = t1;
	                    noCollisions = false;
	                }
	            }
	        }
	        double t3;        // checks manually the last ball
	        if (allBalls.isEmpty()){
	            t3 = dt;
	        } else {
	            t3 = allBalls.get(allBalls.size()-1).collisionTime(this, dt);
	        }
	        
	        if (t3 < earliestTime){
	            Ball[] collision2 = {allBalls.get(allBalls.size()-1)};
	            collision = collision2;
	            earliestTime = t3;
	            noCollisions = false;
	        }
	        if (noCollisions){
	            for (Ball b : allBalls){
	                b.move(dt);
	            }
	            return;
	        }
	        for (Ball b : allBalls){
	            b.move(earliestTime);
	        }
	        if (collision.length == 1){
	            collision[0].collisionWith(this);
	        } 
    		else if (collision.length == 2){
	        	collision[0].collisionWith(collision[1]);
	        }
	        dt -= earliestTime;
	        if (dt == 0){
	        	return;
	        }
    	}
    }

    public ArrayList<Ball> getAllBalls() {
        return allBalls;
    }

    public void setAllBalls(ArrayList<Ball> allBalls) {
        this.allBalls = allBalls;
    }
    
   
    
    @Override
    public Dimension getPreferredSize() {
	return new Dimension(width + 2* borderWidth, height + 2* borderWidth);
    }

    public int getBorderWidth() {
        return borderWidth;
    }
    
    public int getWidth2() {
        return width;
    }
    
    public int getHeight2() {
        return height;
    }
    
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.setColor(Color.BLUE);
        for (Ball ball : allBalls) {
            ball.draw(g);
        }
        for (Ball b : toDraw){
            b.draw(g);
        }
        
        // draws border 
        g.setColor(Color.DARK_GRAY);
        g.fillRect(0, 0, borderWidth, 2*borderWidth + height);
        g.fillRect(0, 0, 2*borderWidth + width, borderWidth);
        g.fillRect(borderWidth + width, 0, borderWidth, 2*borderWidth + height);
        g.fillRect(0, borderWidth + height, 2*borderWidth + width, borderWidth);
    }	
    
    
}

