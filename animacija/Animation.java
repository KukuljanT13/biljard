package animacija;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.Timer;

@SuppressWarnings("serial")
public class Animation extends JFrame implements ActionListener, MouseListener, MouseMotionListener{
	
    private Table table;
    private Timer timer = new Timer(5, this);
    private JButton playButton = new JButton();
    private boolean mousePressed = false;
    private Point2D mouseClick = new Point2D.Double(30, 30);
    JLabel energy;
    JLabel numberOfBalls;

    public Animation(){
            super();
            
            setLayout(new GridBagLayout());
            
            playButton.addActionListener(this);
            GridBagConstraints manager = new GridBagConstraints();
            
            timer.setActionCommand("timer");
            
            manager.gridx = 0;
            manager.gridy = 2;
            manager.weightx = 1;
            manager.gridwidth = 2;
            manager.fill = GridBagConstraints.BOTH;
            
            table = new Table(500, 700);
            table.addMouseListener(this);
            table.addMouseMotionListener(this);
            add(table, manager);
            
            manager.gridx = 0;
            manager.gridy = 0;
            manager.weightx = 1;
            manager.gridwidth = 1;
            manager.fill = 0;
            
            playButton.setActionCommand("play");
            playButton.setText("Play");
            add(playButton, manager);
            
            manager.gridx = 0;
            manager.gridy = 1;
            manager.weightx = 1;
            manager.gridwidth = 0;
            manager.fill = 0;
            
            numberOfBalls = new JLabel("Nr of balls: ");
            add(numberOfBalls, manager);
            
            manager.gridx = 1;
            manager.gridy = 0;
            manager.weightx = 1;
            manager.gridwidth = 1;
            manager.fill = 0;
            
            JButton clearButton = new JButton("Clear");
            clearButton.addActionListener(this);
            clearButton.setActionCommand("clear");
            add(clearButton, manager);
            
            
            Ball b1 = new Ball(100, 200, 1, 0, 0, 0);
            b1.setColor(Color.CYAN);
            Ball b2 = new Ball(400, 200, 1, 0, Math.PI, 0);
            b2.setColor(Color.red);
            table.addBall(b1);
            table.addBall(b2);
            Ball b3 = new Ball(300, 200, 100, 0, 0, 0);
            b3.setColor(Color.white);
            table.addBall(b3);
    }

    public void actionPerformed(ActionEvent e) {
        if ("play".equals(e.getActionCommand())){
            timer.start();
            playButton.setText("Pause");
            playButton.setActionCommand("pause");
        }
        else if ("pause".equals(e.getActionCommand())){
            timer.stop();
            playButton.setText("Play");
            playButton.setActionCommand("play");
        }
        else if ("clear".equals(e.getActionCommand())){
            table.setAllBalls(new ArrayList());
        }
        else if ("timer".equals(e.getActionCommand())){
            table.move(((double)timer.getDelay())/1000);
            repaint();
            int nr = 0;
            for (Ball b : table.getAllBalls()){
                nr ++;
            }
            
            numberOfBalls.setText("Numer of balls:  " + Integer.toString(nr));
        }
    }

    public void mouseClicked(MouseEvent e) {
        int x = e.getX();
        int y = e.getY();
        double angle = new Random().nextDouble();
        Ball b = new Ball(x,y,1, Math.abs((new Random().nextInt()) % 500), angle * Math.PI*2, 0);
        table.addBall(b);
    }

    public void mousePressed(MouseEvent e) {
        this.mousePressed = true;
        mouseClick = new Point2D.Double(e.getX(), e.getY());
        if (table.toDraw.isEmpty()){
        	table.toDraw.add(new Ball(e.getX(), e.getY(), Math.abs(new Random().nextInt() % 10)+1, 0, 0, 0));
        }
    }

    public void mouseReleased(MouseEvent e) {
        this.mousePressed = false;
        
        int x = e.getX();
        int y = e.getY();
        
        Point2D mousePos = new Point2D.Double(x, y);
        double distance = mousePos.distance(mouseClick);
        if (distance == 0){
            return;
        }
        
        if (distance > 500){
            distance = 500;
        }
        
        double angle = Math.atan2((y - mouseClick.getY()), mouseClick.getX() - x);
        if (table.toDraw.isEmpty()){
        	table.toDraw.add(new Ball(mouseClick.getX(), mouseClick.getY(), Math.abs(new Random().nextInt() % 10)+1, 0, 0, 0));
        }
        Ball b = table.toDraw.get(0);
        b.setVelocity(distance);
        b.setVelocityAngle(angle);
        b.setColor(new Color(Math.abs(new Random().nextInt() % 255), Math.abs(new Random().nextInt() % 255), Math.abs(new Random().nextInt() % 255)));
        table.addBall(b);
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }
            
    
    
    public void mouseDragged(MouseEvent e) {
    }

    public void mouseMoved(MouseEvent e) {
        if (this.mousePressed){
        	
        } else{
        	table.toDraw = new ArrayList<Ball>();
        	Ball ball = new Ball(e.getX(), e.getY(), Math.abs(new Random().nextInt() % 10)+1, 0, 0, 0);
        	ball.setColor(new Color(0,0,0,100));
        	table.toDraw.add(ball);
        }
    }
    
    
    
    
    
    public static void main(String[] args){
        Animation a = new Animation();
        a.pack();
        a.setVisible(true);
    }

}
